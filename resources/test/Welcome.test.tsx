import {render} from "@testing-library/react";
import Welcome from "../js/Pages/Welcome";

describe ("Welcome tests", () => {

    let renderComponent: () => any;

    beforeEach(() => {
        renderComponent = () => {
            return render(
                <Welcome/>
            )
        }
    });

    test("renders navbar", () => {
        const {getByTestId} = renderComponent();

        expect(getByTestId("navBarTop")).toBeTruthy();
        expect(getByTestId("navBarTop").textContent).toContain("Home");
        expect(getByTestId("navBarTop").textContent).toContain("About");
        expect(getByTestId("navBarTop").textContent).toContain("Events");
        expect(getByTestId("navBarTop").textContent).toContain("Red Sails Festival");
        expect(getByTestId("navBarTop").textContent).toContain("News");
        expect(getByTestId("navBarTop").textContent).toContain("Contact Us");
        expect(getByTestId("navBarTop").textContent).toContain("Login");
        expect(getByTestId("navBarTop").textContent).toContain("Register");
    });
});
