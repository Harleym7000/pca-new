import TopNav from "../CustomComponents/TopNav";
import {useEffect} from "react";

export default function Welcome() {

    useEffect(() => {
        document.title = "PCA"
    }, []);


    return (
        <>
            <TopNav/>
        </>
    );
}
