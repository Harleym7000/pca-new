import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import {Navbar} from "react-bootstrap";

export default function TopNav() {
    return (
        <>
            <Navbar collapseOnSelect expand="lg" className="shadow-lg" data-testid="navBarTop">
                <Container>
                    <Navbar.Brand href="#home">
                        <img
                            alt="logo"
                            src="/img/pcaLogo.png"
                            width="80"
                            height="60"
                        />{' '}
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto"></Nav>
                        <Nav>
                            <Nav.Link href="/" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>Home</Nav.Link>
                            <Nav.Link eventKey={2} href="/about" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                About
                            </Nav.Link>
                            <Nav.Link eventKey={3} href="/events" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                Events
                            </Nav.Link>
                            <Nav.Link eventKey={3} href="/redSails" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                Red Sails Festival
                            </Nav.Link>
                            <Nav.Link eventKey={4} href="/news" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                News
                            </Nav.Link>
                            <Nav.Link eventKey={5} href="/contactUs" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                Contact Us
                            </Nav.Link>
                            <Nav.Link eventKey={5} href="/login" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                Login
                            </Nav.Link>
                            <Nav.Link eventKey={5} href="/register" className="ml-5" style={{color: "#002080", fontSize: "22px", fontWeight: "700"}}>
                                Register
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
}
